package com.t1.yd.tm.exception.field;

import com.t1.yd.tm.exception.AbstractException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class AbstractFieldException extends AbstractException {

    public AbstractFieldException(@NotNull String message) {
        super(message);
    }

    public AbstractFieldException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(@NotNull String message, @NotNull Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
