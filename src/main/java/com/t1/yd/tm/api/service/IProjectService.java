package com.t1.yd.tm.api.service;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.NotNull;

public interface IProjectService extends IUserOwnedService<Project> {

    void clear(@NotNull String userId);

    @NotNull
    Project create(@NotNull String name);

    @NotNull
    Project create(@NotNull String name, @NotNull String description);

    @NotNull
    Project updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    Project updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description);

    @NotNull
    Project changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Project changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    Project findProjectById(@NotNull String id);

    @NotNull
    Project findProjectById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findProjectByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeProjectById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeProjectByIndex(@NotNull String userId, @NotNull Integer index);

}
