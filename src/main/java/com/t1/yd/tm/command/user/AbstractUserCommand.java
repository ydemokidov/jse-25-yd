package com.t1.yd.tm.command.user;

import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    protected ISaltProvider getSaltProvider() {
        return serviceLocator.getPropertyService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    public void showUser(@NotNull final User user) {
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
    }

}
