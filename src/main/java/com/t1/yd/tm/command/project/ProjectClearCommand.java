package com.t1.yd.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project_clear";

    @NotNull
    public static final String DESCRIPTION = "Clear project";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final String userId = getUserId();
        getProjectService().clear(userId);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }
}
