package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String VERSION_KEY = "application.version";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "author.name";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "author.email";

    @NotNull
    private static final String PWD_ITERATION_DEFAULT = "128";

    @NotNull
    private static final String PWD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PWD_SECRET_DEFAULT = "default";

    @NotNull
    private static final String PWD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    public @NotNull String getApplicationVersion() {
        return getStringValue(VERSION_KEY);
    }

    @Override
    public @NotNull String getAuthorName() {
        return getStringValue(AUTHOR_NAME_KEY);
    }

    @Override
    public @NotNull String getAuthorEmail() {
        return getStringValue(AUTHOR_EMAIL_KEY);
    }

    @Override
    public @NotNull Integer getPasswordIteration() {
        final String result = getStringValue(PWD_ITERATION_KEY, PWD_ITERATION_DEFAULT);
        return Integer.parseInt(result);
    }

    @Override
    public @NotNull String getPasswordSecret() {
        return properties.getProperty(PWD_SECRET_KEY, PWD_SECRET_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().contains(key)) return System.getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

}
